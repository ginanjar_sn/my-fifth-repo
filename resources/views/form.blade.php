<!DOCTYPE html>
<html>
<head>
	<title>SanberBook</title>
</head>
<body>
	<h1>Buat Account Baru!</h1>
	<h2>Sign Up Form</h2>
	<form action="/welcome" method="POST">
    @csrf
	<p>First Name:</p>
	<input type="text" name="first_name">
	<p>Last name:</p>
	<input type="text" name="last_name">
	<p>Gender:</p>
	<input type="radio" name="gender" value="0"> Male<br>
	<input type="radio" name="gender" value="1"> Female<br>
	<input type="radio" name="gender" value="2"> Other
	<br><br>
	<label for="nationality">Nationality:</label>
	<br><br>
	<select>
		<option>Indonesian</option>
		<option>Singaporean</option>
		<option>Malaysian</option>
		<option>Australian</option>
	</select>
	<br><br>
	<label for="language_spoken">Language Spoken:</label>
	<br><br>
	<input type="checkbox" name="language_spoken" value="0"> Bahasa Indonesia<br>
	<input type="checkbox" name="language_spoken" value="1"> English<br>
	<input type="checkbox" name="language_spoken" value="2"> Other
	<br><br>
	<label for="bio">Bio:</label>
	<br><br>
	<textarea cols="40" rows="7"></textarea>
	<br>
	<input type="Submit" value="Sign Up"></input>
	</form>
</body>
</html>